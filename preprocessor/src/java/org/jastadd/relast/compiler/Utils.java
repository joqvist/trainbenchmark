package org.jastadd.relast.compiler;

import static java.util.stream.Collectors.toList;

import java.util.*;
import java.util.function.Predicate;

public class Utils {
	public static <T> List<T> filterToList(Collection<T> collection, Predicate<T> predicate) {
		return collection.stream().filter(predicate).collect(toList());
	}

	public static <T> Set<T> asSet(T... t) {
		return new HashSet<T>(Arrays.asList(t));
	}
}
