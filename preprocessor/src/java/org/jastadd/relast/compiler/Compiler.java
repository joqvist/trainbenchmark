package org.jastadd.relast.compiler;
import org.jastadd.relast.ast.*;

import static org.jastadd.relast.compiler.Utils.filterToList;

import java.io.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.regex.Pattern;
import org.jastadd.relast.compiler.options.CommandLine;
import org.jastadd.relast.compiler.options.EnumOption;
import org.jastadd.relast.compiler.options.FlagOption;
import org.jastadd.relast.compiler.options.Option;
import org.jastadd.relast.compiler.options.StringOption;
import org.jastadd.relast.compiler.options.CommandLine.CommandLineException;

import beaver.Parser;

public class Compiler {
	protected ArrayList<Option<?>> options;
	protected FlagOption optionWriteToFile;
	protected FlagOption optionPrintAST;
	protected StringOption optionListClass;
	protected CommandLine commandLine;

	public Compiler(String args[]) throws CommandLineException {
		options = new ArrayList<>();
		addOptions();

		commandLine = new CommandLine(options);
		commandLine.parse(args);

		if (commandLine.getArguments().size() != 1) {
			error("specify one input file");
		}

		String filename = commandLine.getArguments().get(0);
		Program p = parseProgram(filename);

		if (!p.errors().isEmpty()) {
			if (optionPrintAST.isSet()) {
				System.out.println(p.dumpTree());
			}
			System.err.println("Errors:");
			for (ErrorMessage e: p.errors()) {
				System.err.println(e);
			}
			System.exit(1);
		} else {

			if (optionListClass.isSet()) {
				System.out.println("ListClass is set to " + optionListClass.getValue());
				p.listClass = optionListClass.getValue();
			}

			if (optionWriteToFile.isSet()) {
				File file = new File(filename);
				String absPath = file.getAbsolutePath();
				String absPathExclExt = absPath.substring(0, absPath.lastIndexOf('.'));
				writeToFile(absPathExclExt + "Gen.ast", p.generateAbstractGrammar());
				writeToFile(absPathExclExt + "Gen.jadd", p.generateAspect());
			} else if (optionPrintAST.isSet()) {
				System.out.println(p.dumpTree());
			} else {
				System.out.println(p.generateAbstractGrammar());
				System.out.println(p.generateAspect());
			}
		}
	}

	protected void writeToFile(String filename, String str) {
		try {
			PrintWriter writer = new PrintWriter(filename);
			writer.print(str);
			writer.close();
		} catch (Exception e) {
			e.printStackTrace();
			System.exit(1);
		}
	}

	protected void addOptions() {
		optionWriteToFile = addOption(new FlagOption("file", "write output to files <filename>Gen.ast and <filename>Gen.jadd"));
		optionPrintAST = addOption(new FlagOption("ast", "print AST"));
		optionListClass = addOption(new StringOption("listClass", "determine the class name of the nonterminal reference list"));
	}

	protected <OptionType extends Option<?>> OptionType addOption(OptionType option) {
		options.add(option);
		return option;
	}

	private Program parseProgram(String file) {
		FileReader reader = null;
		try {
			reader = new FileReader(file);
		} catch (FileNotFoundException e) {
			System.err.println(e.getMessage());
			System.exit(1);
		}
		return parse(reader, file);
	}

	private Program parse(Reader reader, String file) {
		RelAstScanner scanner = new RelAstScanner(reader);
		RelAstParser parser = new RelAstParser();

		try {
			return (Program) parser.parse(scanner);
		} catch (IOException e) {
			error(e.getMessage());
		} catch (Parser.Exception e) {
			System.err.println("Parse error in file " + file);
			System.err.println(e.getMessage());
			System.exit(1);
		}
		return null;
	}

	protected void error(String message) {
		System.err.println("Error: " + message);
		System.err.println();
		System.err.println("Usage: java -jar relast-compiler.jar <filename> [--option1] [--option2=value] ...  ");
		System.err.println("Options:");
		System.err.print(commandLine.printOptionHelp());
		System.exit(1);
	}

	public static void main(String[] args) {
		try {
			new Compiler(args);
		} catch (CommandLineException e) {
			System.out.println(e.getMessage());
			System.exit(1);
		}
	}
}

