import AST.*;

public class LowerBounds extends AbstractTests {
	public static void main(String args[]) {
		new LowerBounds().test();
	}

	/*
	 * Root ::= A* B*;
	 * A ::= <Name> [C];
	 * B ::= <Name>;
	 * C ::= <Name>;
	 * rel A.b -> B;
	 * rel B.c <-> C.b;
	 * rel Root.aa? -> A;
	 */
	public void test() {
		Root r = new Root();
		C c1 = new C("c1");
		C c2 = new C("c2");
		A a1 = new A("a1", new Opt<>(c1));
		A a2 = new A("a2", new Opt<>(c2));
		B b1 = new B("b1");
		B b2 = new B("b2");
		r.addA(a1);
		r.addA(a2);
		r.addB(b1);
		r.addB(b2);

		assertTrue(r.violatesLowerBounds());

		a1.setB(b1);
		a2.setB(b2);
		b1.setC(c1);
		b2.setC(c2);

		assertFalse(r.violatesLowerBounds());

		b2.setC(c1);

		assertTrue(r.violatesLowerBounds());

		b1.setC(c2);

		assertFalse(r.violatesLowerBounds());
	}
}