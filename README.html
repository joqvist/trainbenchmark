<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <meta http-equiv="Content-Style-Type" content="text/css" />
  <meta name="generator" content="pandoc" />
  <title></title>
  <style type="text/css">code{white-space: pre;}</style>
  <style type="text/css">
  table {
      border-spacing: 0;
      border-bottom: 2px solid black;
      border-top: 2px solid black;
  }
  table th {
      padding: 3px 10px;
      background-color: white;
      border-top: none;
      border-left: none;
      border-right: none;
      border-bottom: 1px solid black;
  }
  table td {
      padding: 3px 10px;
      border-top: none;
      border-left: none;
      border-bottom: none;
      border-right: none;
  }
  code {
      background-color: lightgrey;
  }
  </style>
</head>
<body>
<h1 id="artifacts-for-continuous-model-validation-using-reference-attribute-grammars">Artifacts for &quot;Continuous Model Validation Using Reference Attribute Grammars&quot;</h1>
<p><em>Note: There is a variant of this submission including a docker image (provided as a link) and one without it (uploaded in HotCRP). We encourage using the one including the image, since building the image takes a long time.</em></p>
<h3 id="authors">Authors</h3>
<ul>
<li>Johannes Mey <a href="mailto:johannes.mey@tu-dresden.de">johannes.mey@tu-dresden.de</a></li>
<li>Carl Mai <a href="mailto:carl.mai@tu-dresden.de">carl.mai@tu-dresden.de</a></li>
<li>René Schöne <a href="mailto:rene.schoene@tu-dresden.de">rene.schoene@tu-dresden.de</a></li>
<li>Görel Hedin <a href="mailto:gorel.hedin@cs.lth.se">gorel.hedin@cs.lth.se</a></li>
<li>Emma Söderberg <a href="mailto:emma.soderberg@cs.lth.se">emma.soderberg@cs.lth.se</a></li>
<li>Thomas Kühn <a href="mailto:thomas.kuehn3@tu-dresden.de">thomas.kuehn3@tu-dresden.de</a></li>
<li>Niklas Fors <a href="mailto:niklas.fors@cs.lth.se">niklas.fors@cs.lth.se</a></li>
<li>Jesper Öqvist <a href="mailto:jesper.oqvist@cs.lth.se">jesper.oqvist@cs.lth.se</a></li>
<li>Uwe Aßmann <a href="mailto:uwe.assmann@tu-dresden.de">uwe.assmann@tu-dresden.de</a></li>
</ul>
<h3 id="introduction">Introduction</h3>
<p>The paper discusses the utilization of reference attribute grammars (RAGs) for model validation and presents two specific contributions. First, the differences between models and trees specified by reference attribute grammars, specifically non-containment references, are discussed and a manual, yet optimised method to efficiently overcome these differences is presented. Secondly, an extension of RAG grammar specifications is proposed to model non-containment references automatically. The proposed modelling techniques are compared to state-of-the-art modelling tools utilizing a benchmarking framework for continuous model validation, the <em>Train Benchmark</em>.</p>
<h3 id="structure-of-the-supplementary-artifacts">Structure of the Supplementary Artifacts</h3>
<p>The artifacts are structured in three parts:</p>
<ul>
<li>A standalone example of the non-containment references preprocessor</li>
<li>Benchmark code to reproduce the measurements, including all relevant source codes</li>
<li>Full collection of all measurement data and diagrams mentioned in the paper</li>
</ul>
<h3 id="general-remarks-on-the-presented-listings-and-measurements">General Remarks on the presented Listings and Measurements</h3>
<p>For reasons of readability and simplicity, there are some minor differences in naming in the source codes and the measured resulting data. Most importantly, the names of the three presented JastAdd implementation variants are different in the code and the diagrams.</p>
<p>The following table shows the relation of the terminology used in the paper and in the code.</p>
<table>
<thead>
<tr class="header">
<th>Name used in Paper</th>
<th>Name used in result data</th>
<th>Name used in source code</th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td>Name Lookup</td>
<td>Jastadd (Name Lookup)</td>
<td>jastadd-namelookup</td>
</tr>
<tr class="even">
<td>Intrinsic References</td>
<td>Jastadd (Optimized)</td>
<td>jastadd-optimized</td>
</tr>
<tr class="odd">
<td>Grammar Extension</td>
<td>Jastadd (Specialized)</td>
<td>jastadd-specialized</td>
</tr>
</tbody>
</table>
<h2 id="the-grammar-extension-preprocessor-relast">The Grammar Extension Preprocessor <em>RelAst</em></h2>
<p>To transform the grammar extension we provide a preprocessor for JastAdd. This preprocessor including its source code is provided in the <code>preprocessor</code> subdirectory.</p>
<p>Its usage is:</p>
<ul>
<li>Run preprocessor on train benchmark (output written to standard output):
<ul>
<li><code>cat examples/TrainBenchmark.relast</code></li>
<li><code>java -jar relast-compiler.jar examples/TrainBenchmark.relast</code></li>
</ul></li>
<li>Run preprocessor and write output to files:
<ul>
<li><code>java -jar relast-compiler.jar examples/TrainBenchmark.relast --file</code></li>
<li><code>cat examples/TrainBenchmarkGen.ast</code></li>
<li><code>cat examples/TrainBenchmarkGen.jadd</code></li>
</ul></li>
<li>Run preprocessor and write output to files (with a different list class):
<ul>
<li><code>java -jar relast-compiler.jar examples/TrainBenchmark.relast --listClass=MyListClass --file</code></li>
<li><code>cat examples/TrainBenchmarkGen.ast</code></li>
<li><code>cat examples/TrainBenchmarkGen.jadd</code></li>
</ul></li>
</ul>
<h2 id="the-train-benchmark">The Train Benchmark</h2>
<h3 id="structure-of-the-train-benchmark">Structure of the Train Benchmark</h3>
<p>The benchmark is able to measure different scenarios specified by configurations with several kinds of parameters:</p>
<ol style="list-style-type: decimal">
<li><strong>Input Data:</strong> There are two types of input data used in the benchmark, the <code>inject</code> and the <code>repair</code> data set. The former contains <em>valid</em> models, i.e., models, which do not contain any of the faults that are supposed to be found by the presented queries. The latter, <code>repair</code>, contains models already containing faults.</li>
<li><strong>Queries:</strong> The queries are used to find the aforementioned faults. For each fault, there are two queries: <em>repair</em>, to find the fault, and <em>inject</em>, to find places where a fault can be injected.</li>
<li><strong>Transformations:</strong> The transformations performed by the benchmark are, again, two sets: <em>inject</em> and <em>repair</em> transformations.</li>
<li><strong>Transformation Strategies:</strong> The benchmark does not perform the operation on all matches. The strategy <em>fixed</em> performs the transformation on a given number of matches, while the <em>proportional</em> strategy performs them on a given percentage of all matches.</li>
</ol>
<p>These settings are defined in a <em>benchmark scenario</em>, which can be edited before running the benchmark.</p>
<h3 id="measurement-data">Measurement Data</h3>
<p>The result data is stored in the directory <a href="paper-results/" class="uri">paper-results/</a>. This directory contains two subdirectories:</p>
<ul>
<li><a href="paper-results/measurements">measurements</a> contains two directories. The <a href="paper-results/measurements/inject">inject</a> subdirectory contains the measurements for the <em>inject</em> scenario, which is also included in <a href="paper-results/measurements/inject/BenchmarkScript.groovy">inject/BenchmarkScript.groovy</a>. The <a href="paper-results/measurements/repair">repair</a> subdirectory contains the same data for the <em>repair</em> scenario in <a href="paper-results/measurements/repair/BenchmarkScript.groovy">repair/BenchmarkScript.groovy</a>. Both directories contain files with time measurement data (starting with <code>times</code>) and the numbers of matches (starting with <code>matches</code>). Each file name contains information on the tool used, the query, and the size of the model.</li>
<li><a href="paper-results/diagrams">diagrams</a> contains the same subdirectories, both containing diagrams with the respective measurements. The diagrams are generated from the same data as in the paper, but enlarged for better readability. In particular, the six diagrams presented in the paper are
<ul>
<li><a href="paper-results/diagrams/repair/Read-and-Check-RouteSensor.pdf">Fig. 7a. Read and Check for RouteSensor (repair)</a></li>
<li><a href="paper-results/diagrams/repair/Read-and-Check-ConnectedSegments.pdf">Fig. 7b. Read and Check for ConnectedSegments (repair)</a></li>
<li><a href="paper-results/diagrams/inject/Transformation-and-Recheck-RouteSensor.pdf">Fig. 7c. Transformation and Recheck for RouteSensor (inject)</a></li>
<li><a href="paper-results/diagrams/inject/Transformation-and-Recheck-ConnectedSegments.pdf">Fig. 7d. Transformation and Recheck for ConnectedSegments (inject)</a></li>
<li><a href="paper-results/diagrams/repair/Transformation-and-Recheck-RouteSensor.pdf">Fig. 7e. Transformation and Recheck for RouteSensor (repair)</a></li>
<li><a href="paper-results/diagrams/repair/Transformation-and-Recheck-ConnectedSegments.pdf">Fig. 7f. Transformation and Recheck for ConnectedSegments (repair)</a></li>
</ul></li>
</ul>
<p><strong>Please Note:</strong> The measurements were conducted using a timeout for the whole run. If a run was not completed, no individual times of the steps appear in the measurements and diagrams. Thus, some tools do not have measurements for all problem sizes.</p>
<h3 id="the-source-code">The Source Code</h3>
<p>For this publication, we tried to modify the source code of the benchmark itself as little as possible. Therefore, unfortunately, the code base is rather large and confusing. The following section tries to point to the parts relevant for this paper.</p>
<p>The benchmark is structured in modules, some of which form the code of the benchmark, some are provided by the contesting tools, and some are related to required model serializations. There are some naming conventions: - Tool-related modules are in directories starting with <code>trainbenchmark-tool</code>. - Model serialization-related modules start with <code>trainbenchmark-generator</code>. - All other modules are core modules of the benchmark.</p>
<p>The JastAdd-based solutions use a preprocessor to generate Java files, for the presented variant. Each JastAdd configuration must be presented to the benchmark as a separate tool. Thus, there are two directories for each variant, one for the batch processing mode and one for the incremental mode. Because these two modes share almost all the source code, a third directory is used to store this shared code. Finally, there is a directory for code shared between all JastAdd variants. These are the important directories:</p>
<ul>
<li><a href="trainbenchmark/trainbenchmark-tool-jastadd-namelookup-base">JastAdd with Name Lookup</a>
<ul>
<li><a href="trainbenchmark/trainbenchmark-tool-jastadd-namelookup-base/src/main/jastadd/train.ast">Grammar</a></li>
<li><a href="trainbenchmark/trainbenchmark-tool-jastadd-namelookup-base/src/main/jastadd/queries">Queries</a></li>
<li><a href="trainbenchmark/trainbenchmark-tool-jastadd-namelookup-base/src/main/java/de/tudresden/inf/st/train/jastadd/transformations">Transformations</a></li>
</ul></li>
<li><a href="trainbenchmark/trainbenchmark-tool-jastadd-optimized-base">JastAdd with Intrinsic References</a>
<ul>
<li><a href="trainbenchmark/trainbenchmark-tool-jastadd-optimized-base/src/main/jastadd/train.ast">Grammar</a></li>
<li><a href="trainbenchmark/trainbenchmark-tool-jastadd-optimized-base/src/main/jastadd/queries">Queries</a></li>
<li><a href="trainbenchmark/trainbenchmark-tool-jastadd-optimized-base/src/main/java/de/tudresden/inf/st/train/jastadd/transformations">Transformations</a></li>
</ul></li>
<li><a href="trainbenchmark/trainbenchmark-tool-jastadd-specialized-base">JastAdd with Grammar Extension</a>
<ul>
<li><a href="trainbenchmark/trainbenchmark-tool-jastadd-specialized-base/src/main/jastadd/Train.relast">(Extended) Grammar</a></li>
<li><a href="trainbenchmark/trainbenchmark-tool-jastadd-specialized-base/src/main/jastadd/queries">Queries</a></li>
<li><a href="trainbenchmark/trainbenchmark-tool-jastadd-specialized-base/src/main/java/de/tudresden/inf/st/train/jastadd/transformations">Transformations</a></li>
</ul></li>
<li><a href="trainbenchmark/trainbenchmark-tool-jastadd-base">Common JastAdd Code</a></li>
</ul>
<h3 id="reproducing-the-measurements">Reproducing the Measurements</h3>
<p><strong><span style="color:red">Please Note: Reproducing the graphs as presented in the paper and supplied here takes a very long time depending on the utilized hardware. It is strongly suggested running the benchmark with a smaller maximum problem size, fewer repetitions, and a shorter timeout.</span></strong> Most results of the benchmark are observable with more restricted setup as well. In the following, we will provide a suggested way to run the benchmark in different sizes. Note that running the benchmark requires a significant amount of disk space (up to 10GB when running the full benchmark).</p>
<p>To reproduce the measurements, there are several options. We provide a prepared Docker image that can be run directly. Alternatively, it is, of course, also possible to simply run the provided gradle build scripts. However, since there are some software requirements imposed by the benchmark, particularly for creating the diagrams using R. We strongly suggest running the Docker variant.</p>
<h4 id="running-the-benchmark-with-docker">Running the Benchmark with Docker</h4>
<h5 id="loading-the-docker-image">Loading the Docker Image</h5>
<ul>
<li>Variant 1 (<em>recommended</em>): Load the provided docker image
<ul>
<li>Prerequisites: An installation of Docker in the <code>PATH</code></li>
<li>Steps:
<ul>
<li>Unpack the provided archive and open a terminal in the extracted directory</li>
<li><code>docker load --input trainbenchmark-docker.tar</code></li>
</ul></li>
</ul></li>
<li>Variant 2: Build the docker image from the provided Dockerfile
<ul>
<li>Prerequisites: An installation of Docker in the <code>PATH</code></li>
<li>Steps:
<ul>
<li>Unpack the provided archive and open a terminal in the extracted directory</li>
<li><code>docker build -t trainbenchmark .</code></li>
</ul></li>
</ul></li>
</ul>
<h5 id="running-the-docker-image">Running the Docker Image</h5>
<ul>
<li><code>docker run -it -v &quot;$PWD&quot;/docker-results:/trainbenchmark/results:Z -v &quot;$PWD&quot;/docker-diagrams:/trainbenchmark/diagrams:Z trainbenchmark</code></li>
<li>This makes the results and diagrams available outside the container in the directories <code>docker-results</code> and <code>docker-diagrams</code> respectively</li>
<li>Once running, a command prompt is opened and some information is displayed</li>
<li>Follow the instructions below</li>
</ul>
<h4 id="running-the-benchmark-directly">Running the Benchmark directly</h4>
<ul>
<li>For running a standard run, use one of the following commands:</li>
</ul>
<table>
<thead>
<tr class="header">
<th>Name</th>
<th>Command</th>
<th>Minimum size</th>
<th>Maximum size</th>
<th>Timeout</th>
<th>Runs</th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td>Small</td>
<td><code>./run_small</code></td>
<td>1</td>
<td>32</td>
<td>60s</td>
<td>1</td>
</tr>
<tr class="even">
<td>Medium</td>
<td><code>./run_medium</code></td>
<td>1</td>
<td>64</td>
<td>10min</td>
<td>5</td>
</tr>
<tr class="odd">
<td>Full</td>
<td><code>./run_full</code></td>
<td>1</td>
<td>512</td>
<td>15min</td>
<td>10</td>
</tr>
</tbody>
</table>
<ul>
<li>For running a custom run,
<ul>
<li>run <code>./gradlew preprocess</code> to generate the grammar from the extended grammar specification</li>
<li>run <code>./gradlew build shadowJar -x test</code></li>
<li>run <code>./gradlew initScripts</code></li>
<li>configure the scripts by running <code>./scripts/configure.sh 1 &lt;MAXSIZE&gt; &lt;TIMEOUT in s&gt; &lt;REPETITIONS&gt;</code>
<ul>
<li>Where MAXSIZE is one of 2, 4, 8, 16, 32, 64, 128, 256, 512, or, 1024. The larger sizes use <strong>a lot of</strong> disk space!</li>
</ul></li>
<li>run <code>./gradlew initScripts</code></li>
<li>run <code>./gradlew generate</code></li>
<li>run the benchmark
<ul>
<li>run <code>./gradlew individualInjectBenchmark</code> for the <em>inject</em> scenarios</li>
<li>run <code>./gradlew individualRepairBenchmark</code> for the <em>repair</em> scenarios</li>
</ul></li>
<li>Plot the diagrams for the current run: <code>./gradlew plotIndividual</code></li>
</ul></li>
<li>The resulting data and diagrams is placed in the <code>results</code> and the <code>diagrams</code> folder
<ul>
<li>When running with docker, the data is also in <code>docker-results</code> and <code>docker-diagrams</code> on the host machine.</li>
</ul></li>
</ul>
</body>
</html>
