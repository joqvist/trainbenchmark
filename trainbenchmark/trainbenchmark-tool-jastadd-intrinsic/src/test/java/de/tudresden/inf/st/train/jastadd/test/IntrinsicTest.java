package de.tudresden.inf.st.train.jastadd.test;

import de.tudresden.inf.st.train.jastadd.IntrinsicBenchmarkScenario;
import de.tudresden.inf.st.train.jastadd.config.IntrinsicBenchmarkConfig;
import de.tudresden.inf.st.train.jastadd.config.IntrinsicBenchmarkConfigBuilder;
import hu.bme.mit.trainbenchmark.benchmark.config.BenchmarkConfigBase;
import hu.bme.mit.trainbenchmark.benchmark.runcomponents.BenchmarkResult;
import hu.bme.mit.trainbenchmark.benchmark.test.TrainBenchmarkTest;

public class IntrinsicTest extends TrainBenchmarkTest {

  @Override
  protected BenchmarkResult runTest(final BenchmarkConfigBase bcb) throws Exception {
    final IntrinsicBenchmarkConfig bc = new IntrinsicBenchmarkConfigBuilder().setConfigBase(bcb).createConfig();
    final IntrinsicBenchmarkScenario scenario = new IntrinsicBenchmarkScenario(bc);
    final BenchmarkResult result = scenario.performBenchmark();
    return result;
  }

}
