package de.tudresden.inf.st.train.jastadd.config;

import hu.bme.mit.trainbenchmark.benchmark.config.BenchmarkConfig;
import hu.bme.mit.trainbenchmark.benchmark.config.BenchmarkConfigBase;

public class IntrinsicBenchmarkConfig extends JastaddAbstractBenchmarkConfig {

  protected IntrinsicBenchmarkConfig(final BenchmarkConfigBase configBase) {
    super(configBase);
  }

  @Override
  public String getToolName() {
    return "Intrinsic References";
  }

  @Override
  public String getProjectName() {
    return "jastadd-intrinsic";
  }

}
