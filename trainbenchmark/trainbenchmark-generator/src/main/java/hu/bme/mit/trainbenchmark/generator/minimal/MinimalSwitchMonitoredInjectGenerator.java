package hu.bme.mit.trainbenchmark.generator.minimal;

import java.io.IOException;

import hu.bme.mit.trainbenchmark.constants.Position;
import hu.bme.mit.trainbenchmark.generator.ModelSerializer;
import hu.bme.mit.trainbenchmark.generator.config.GeneratorConfig;

import static hu.bme.mit.trainbenchmark.constants.ModelConstants.*;

public class MinimalSwitchMonitoredInjectGenerator extends MinimalModelGenerator {

	public MinimalSwitchMonitoredInjectGenerator(final ModelSerializer<?> serializer, final GeneratorConfig generatorConfig) {
		super(serializer, generatorConfig);
	}

	@Override
	protected void buildPatternModel() throws IOException {
		final Object region = serializer.createVertex(REGION);
		final Object sw = serializer.createVertex(SWITCH);
		serializer.setAttribute(SWITCH, sw, CURRENTPOSITION, Position.STRAIGHT);
		final Object sensor = serializer.createVertex(SENSOR);
		serializer.createEdge(SENSORS, region, sensor);
		serializer.createEdge(ELEMENTS, region, sw);
		serializer.createEdge(MONITORED_BY, sw, sensor);
	}

}
