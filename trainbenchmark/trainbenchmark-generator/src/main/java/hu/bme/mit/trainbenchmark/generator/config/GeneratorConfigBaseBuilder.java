package hu.bme.mit.trainbenchmark.generator.config;

import com.google.common.base.Preconditions;

public final class GeneratorConfigBaseBuilder {
	private Scenario scenario;
	private Integer size;
	private ModelType type;

	public GeneratorConfigBaseBuilder setScenario(Scenario scenario) {
		this.scenario = scenario;
		return this;
	}

	public GeneratorConfigBaseBuilder setSize(int size) {
		this.size = size;
		return this;
	}

	public GeneratorConfigBaseBuilder setModelType(ModelType modelType) {
		this.type = modelType;
		return this;
	}

	public GeneratorConfigBase createGeneratorConfigBase() {
		Preconditions.checkNotNull(scenario);
		Preconditions.checkNotNull(size);
		if (size == 0) {
			Preconditions.checkNotNull(type);
		}
		return new GeneratorConfigBase(scenario, size, type);
	}

}
