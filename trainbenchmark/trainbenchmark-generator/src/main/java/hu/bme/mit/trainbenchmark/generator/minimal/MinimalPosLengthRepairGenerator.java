package hu.bme.mit.trainbenchmark.generator.minimal;

import com.google.common.collect.ImmutableMap;
import hu.bme.mit.trainbenchmark.generator.ModelSerializer;
import hu.bme.mit.trainbenchmark.generator.config.GeneratorConfig;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Map;

import static hu.bme.mit.trainbenchmark.constants.ModelConstants.*;

public class MinimalPosLengthRepairGenerator extends MinimalModelGenerator {

	public MinimalPosLengthRepairGenerator(final ModelSerializer<?> serializer, final GeneratorConfig generatorConfig) {
		super(serializer, generatorConfig);
	}

	@Override
	protected void buildPatternModel() throws FileNotFoundException, IOException {
		final Object region = serializer.createVertex(REGION);

		final Map<String, ? extends Object> segmentAttributes = ImmutableMap.of(LENGTH, -1);
		final Object segment = serializer.createVertex(SEGMENT, segmentAttributes);
		serializer.createEdge(ELEMENTS, region, segment);
	}

}
