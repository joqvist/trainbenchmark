package de.tudresden.inf.st.trainbenchmark.generator.jastadd;

import de.tudresden.inf.st.train.jastadd.ast.*;
import de.tudresden.inf.st.trainbenchmark.generator.jastadd.config.RelASTGeneratorConfig;
import hu.bme.mit.trainbenchmark.generator.ModelSerializer;

import java.io.File;
import java.io.IOException;
import java.util.Map;

import static hu.bme.mit.trainbenchmark.constants.ModelConstants.*;

public class RelASTSerializer extends ModelSerializer<RelASTGeneratorConfig> {

  private RailwayContainer container;

  RelASTSerializer(final RelASTGeneratorConfig generatorConfig) {
    super(generatorConfig);
  }

  @Override
  public String syntax() {
    return "JASTADD-RELAST";
  }

  @Override
  public void initModel() {
    container = new RailwayContainer();
  }

  @Override
  public void persistModel() throws IOException {
    String jsonPath = gc.getConfigBase().getModelPathWithoutExtension() + "-jastadd-relast.json";
    System.out.println(jsonPath);
    try {
      container.resolveAll();
      container.serialize(new File(jsonPath), true);
    } catch (SerializationException e) {
      throw new IOException(e);
    }
  }

  @Override
  public ASTNode createVertex(
    final int id,
    final String type,
    final Map<String, ?> attributes,
    final Map<String, Object> outgoingEdges,
    final Map<String, Object> incomingEdges) throws IOException {

    switch (type) {
      case REGION:
        Region region = new Region();
        region.setId(id);
        container.addRegion(region);
        return region;
      case SEMAPHORE:
        Semaphore semaphore = new Semaphore();
        semaphore.setId(id);
        if (attributes.get(SIGNAL) != null) {
          semaphore.setSignal(Signal.valueOf(((hu.bme.mit.trainbenchmark.constants.Signal) attributes.get(SIGNAL)).name()));
        }
        return semaphore;
      case ROUTE:
        Route route = new Route();
        route.setId(id);
        if (attributes.get(ACTIVE) != null) {
          route.setActive((Boolean) attributes.get(ACTIVE));
        }
        if (outgoingEdges.get(ENTRY) != null) {
          route.setEntry((Semaphore) outgoingEdges.get(ENTRY));
        }
        if (outgoingEdges.get(EXIT) != null) {
          route.setExit((Semaphore) outgoingEdges.get(EXIT));
        }
        container.addRoute(route);
        return route;
      case SWITCHPOSITION:
        SwitchPosition switchPosition = new SwitchPosition();
        switchPosition.setId(id);
        if (attributes.get(POSITION) != null) {
          switchPosition.setPosition(Position.valueOf(((hu.bme.mit.trainbenchmark.constants.Position) attributes.get(POSITION)).name()));
        }
        if (outgoingEdges.containsKey(TARGET)) {
          switchPosition.setTarget((Switch) outgoingEdges.get(TARGET));
        }
        return switchPosition;
      case SENSOR:
        Sensor sensor = new Sensor();
        sensor.setId(id);
        return sensor;
      case SEGMENT:
        Segment segment = new Segment();
        segment.setId(id);
        if (attributes.get(LENGTH) != null) {
          segment.setLength((Integer) attributes.get(LENGTH));
        }
        return segment;
      case SWITCH:
        Switch sw = new Switch();
        sw.setId(id);
        if (attributes.get(POSITION) != null) {
          sw.setCurrentPosition(Position.valueOf(((hu.bme.mit.trainbenchmark.constants.Position) attributes.get(POSITION)).name()));
        }
        return sw;
      default:
        throw new IOException(String.format("Unknown type of vertex: %s", type));
    }
  }

  @Override
  public void createEdge(final String label, final Object from, final Object to) throws IOException {

    switch (label) {
      case MONITORED_BY: {
        TrackElement trackElement = (TrackElement) from;
        Sensor sensor = (Sensor) to;
        trackElement.addToMonitoredBy(sensor);
        break;
      }
      case REQUIRES: {
        Route route = (Route) from;
        Sensor sensor = (Sensor) to;
        route.addToRequires(sensor);
        break;
      }
      case CONNECTS_TO:
        TrackElement from_track = (TrackElement) from;
        TrackElement to_track = (TrackElement) to;
        from_track.addToConnectsTo(to_track);
        break;
      case ELEMENTS: {
        Region region = (Region) from;
        TrackElement trackElement = (TrackElement) to;
        region.addTrackElement(trackElement);
        break;
      }
      case FOLLOWS: {
        Route route = (Route) from;
        SwitchPosition switchPosition = (SwitchPosition) to;
        route.addSwitchPosition(switchPosition);
        break;
      }
      case SEMAPHORES:
        Segment segment = (Segment) from;
        Semaphore semaphore = (Semaphore) to;
        segment.addSemaphore(semaphore);
        break;
      case SENSORS: {
        Region region = (Region) from;
        Sensor sensor = (Sensor) to;
        region.addSensor(sensor);
        break;
      }
      default:
        throw new IOException("An edge could not be created because '" + label + "' is an unknown label.");
    }
  }

  @Override
  public void setAttribute(final String type, final Object node, final String key, final Object value)
    throws IOException {
    if (key.equals(CURRENTPOSITION)) {
      Switch sw = (Switch) node;
      sw.setCurrentPosition(Position.valueOf(((hu.bme.mit.trainbenchmark.constants.Position) value).name()));
    } else {
      throw new IOException(String.format("SetAttribute. Unexpected key: %s", type));
    }
  }

}
