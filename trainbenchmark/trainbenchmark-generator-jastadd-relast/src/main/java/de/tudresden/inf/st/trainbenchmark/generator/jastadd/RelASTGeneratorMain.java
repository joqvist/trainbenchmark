package de.tudresden.inf.st.trainbenchmark.generator.jastadd;

import de.tudresden.inf.st.trainbenchmark.generator.jastadd.config.RelASTGeneratorConfig;
import hu.bme.mit.trainbenchmark.generator.ModelGenerator;
import hu.bme.mit.trainbenchmark.generator.ScalableGeneratorFactory;
import hu.bme.mit.trainbenchmark.generator.config.GeneratorConfig;

public class RelASTGeneratorMain {
  public static void main(String[] args) throws Exception {
    final RelASTGeneratorConfig gc = GeneratorConfig.fromFile(args[0], RelASTGeneratorConfig.class);
    final RelASTSerializer serializer = new RelASTSerializer(gc);
    final ModelGenerator generator = ScalableGeneratorFactory.createGenerator(serializer, gc);
    generator.generateModel();
  }
}
