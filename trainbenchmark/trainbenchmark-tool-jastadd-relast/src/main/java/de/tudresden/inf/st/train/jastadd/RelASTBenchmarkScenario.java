package de.tudresden.inf.st.train.jastadd;

import de.tudresden.inf.st.train.jastadd.config.RelASTBenchmarkConfig;

public class RelASTBenchmarkScenario extends
  JastaddAbstractBenchmarkScenario<RelASTBenchmarkConfig> {

  public RelASTBenchmarkScenario(final RelASTBenchmarkConfig bc) throws Exception {
    super(bc, true);
  }

}
