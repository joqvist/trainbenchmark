package de.tudresden.inf.st.train.jastadd.config;

import hu.bme.mit.trainbenchmark.benchmark.config.BenchmarkConfigBase;

public class RelASTBenchmarkConfig extends JastaddAbstractBenchmarkConfig {

  protected RelASTBenchmarkConfig(final BenchmarkConfigBase configBase) {
    super(configBase);
  }

  @Override
  public String getToolName() {
    return "Grammar Extension with Serializer";
  }

  @Override
  public String getProjectName() {
    return "jastadd-relast";
  }

}
