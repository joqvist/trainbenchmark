package de.tudresden.inf.st.train.jastadd.config;

import hu.bme.mit.trainbenchmark.benchmark.config.BenchmarkConfigBuilder;

public class RelASTBenchmarkConfigBuilder extends BenchmarkConfigBuilder<RelASTBenchmarkConfig, RelASTBenchmarkConfigBuilder> {

  @Override
  public RelASTBenchmarkConfig createConfig() {
    checkNotNulls();
    return new RelASTBenchmarkConfig(configBase);
  }

}
