package de.tudresden.inf.st.train.jastadd;

import de.tudresden.inf.st.train.jastadd.config.RelASTIncrementalBenchmarkConfig;

public class RelASTIncrementalBenchmarkScenario extends
  JastaddAbstractBenchmarkScenario<RelASTIncrementalBenchmarkConfig> {

  public RelASTIncrementalBenchmarkScenario(final RelASTIncrementalBenchmarkConfig bc) throws Exception {
    super(bc, false);
  }

}
