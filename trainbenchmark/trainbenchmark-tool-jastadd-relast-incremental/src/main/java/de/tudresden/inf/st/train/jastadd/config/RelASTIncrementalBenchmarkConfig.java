package de.tudresden.inf.st.train.jastadd.config;

import hu.bme.mit.trainbenchmark.benchmark.config.BenchmarkConfigBase;

public class RelASTIncrementalBenchmarkConfig extends JastaddAbstractBenchmarkConfig {

  protected RelASTIncrementalBenchmarkConfig(final BenchmarkConfigBase configBase) {
    super(configBase);
  }

  @Override
  public String getToolName() {
    return "Grammar Extension with Serializer (Incremental)";
  }

  @Override
  public String getProjectName() {
    return "jastadd-relast-incremental";
  }

}
