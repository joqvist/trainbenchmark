#!/bin/bash

cd "$( cd "$( dirname "$0" )" && pwd )/../"||exit 1

MINSIZE="$1"
MAXSIZE="$2"
TIMEOUT="$3"
RUNS="$4"

if [ -z "$RUNS" ]
then
    echo "Usage: ./scripts/configure.sh min max timeout runs"
    echo "Will configure the generateor and benchmark to use problems between sizes min and max"
    echo "The benchmark receives a timeout in seconds and the amount of runs"
    echo "Default is: ./scripts/configure.sh 1 512 900 10"
    exit 1
fi
echo "Configuring the source with:"
echo "minSize: $MINSIZE"
echo "maxSize: $MAXSIZE"
echo "timeout: $TIMEOUT"
echo "runs: $RUNS"
echo -e "\\033[1;96mRemember that this only configures and initializes. Further steps:"
echo -e "\\033[1;91mGenerate:"
echo -e "\\033[0m  if not yet done run ./gradlew generate"
echo "     but beware: maxSize 1024 -> ~40GB of data"
echo -e "\\033[1;91mBenchmark:"
echo -e "\\033[0m  ./gradlew individualInjectBenchmark"
echo "  ./gradlew individualRepairBenchmark"

sed -i "s/^def minSize =.*/def minSize = $MINSIZE/" ./trainbenchmark-scripts/src-template/GeneratorScript.groovy
sed -i "s/^def maxSize =.*/def maxSize = $MAXSIZE/" ./trainbenchmark-scripts/src-template/GeneratorScript.groovy
sed -i "s/^def minSize =.*/def minSize = $MINSIZE/" ./trainbenchmark-scripts/src-template/IndividualBenchmarkInjectScript.groovy
sed -i "s/^def maxSize =.*/def maxSize = $MAXSIZE/" ./trainbenchmark-scripts/src-template/IndividualBenchmarkInjectScript.groovy
sed -i "s/^def timeout =.*/def timeout = $TIMEOUT/" ./trainbenchmark-scripts/src-template/IndividualBenchmarkInjectScript.groovy
sed -i "s/^def runs =.*/def runs = $RUNS/" ./trainbenchmark-scripts/src-template/IndividualBenchmarkInjectScript.groovy
sed -i "s/^def minSize =.*/def minSize = $MINSIZE/" ./trainbenchmark-scripts/src-template/IndividualBenchmarkRepairScript.groovy
sed -i "s/^def maxSize =.*/def maxSize = $MAXSIZE/" ./trainbenchmark-scripts/src-template/IndividualBenchmarkRepairScript.groovy
sed -i "s/^def timeout =.*/def timeout = $TIMEOUT/" ./trainbenchmark-scripts/src-template/IndividualBenchmarkRepairScript.groovy
sed -i "s/^def runs =.*/def runs = $RUNS/" ./trainbenchmark-scripts/src-template/IndividualBenchmarkRepairScript.groovy
./gradlew --no-daemon initScripts
