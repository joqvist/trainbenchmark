import argparse
import json
import logging
import os.path
import subprocess


FORMAT = '%(asctime)s %(levelname)-8s %(threadName)-10s (%(filename)s:%(lineno)d): %(message)s'
logger = logging.getLogger('toolwise')


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Plot results per tool.')
    parser.add_argument(
        "-v", "--verbose", help="Print debug messages.", action="store_true")
    parser.add_argument(
        "-f", "--file-config", default='merge_results.json', help="Config file to use.")
    args = parser.parse_args()
    logging.basicConfig(format=FORMAT, level=logging.DEBUG if args.verbose else logging.INFO)
    # load config file
    with open('merge_results.json') as fdr:
        content = json.load(fdr)
    # update with local version, if existing
    directory, basename = os.path.split(os.path.abspath('merge_results.json'))
    local_config_file = os.path.join(directory, 'local-' + basename)
    if os.path.exists(local_config_file):
        with open(local_config_file) as fdr:
            content.update(json.load(fdr))
    else:
        logger.debug('No local config file found.')
    for tool in content.get('toolwise', []):
        logging.info('Processing %s now.', tool)
        subprocess.call(["Rscript", "toolwise.R", tool])
