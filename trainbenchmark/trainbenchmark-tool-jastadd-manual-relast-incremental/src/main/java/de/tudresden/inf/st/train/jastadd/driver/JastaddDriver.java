package de.tudresden.inf.st.train.jastadd.driver;

import de.tudresden.inf.st.train.jastadd.ast.ASTState;
import de.tudresden.inf.st.train.jastadd.ast.RailwayContainer;
import de.tudresden.inf.st.train.jastadd.driver.deserializer.JsonDeserializer;
import hu.bme.mit.trainbenchmark.benchmark.driver.Driver;

import java.io.File;
import java.io.IOException;


public class JastaddDriver extends Driver {

  private final boolean flushCaches;
  private final ASTState.Trace.Receiver receiver;
  RailwayContainer root;
  private int idCounter = 0;
  public JastaddDriver(boolean flushCaches, ASTState.Trace.Receiver receiver) {
    super();
    this.flushCaches = flushCaches;
    this.receiver = receiver;
  }

  public static JastaddDriver create(boolean flushCaches, ASTState.Trace.Receiver receiver) {
    return new JastaddDriver(flushCaches, receiver);
  }

  public boolean flushCaches() {
    return flushCaches;
  }

  public int nextId() {
    idCounter--;
    return idCounter;
  }

  public RailwayContainer getModel() {
    return root;
  }

  @Override
  public void read(final String modelPath) throws IOException {

    File modelFile = new File(modelPath);
    root = JsonDeserializer.read(modelFile);

    // enable tracing if there is a receiver
    if (receiver != null) {
      getModel().trace().setReceiver(receiver);
    }
  }

  @Override
  public String getPostfix() {
    return "-jastadd-manual-intrinsic.json";
  }

  public void flushCache() {
    if (flushCaches) {
      getModel().flushTreeCache();
    }
  }
}
