package de.tudresden.inf.st.train.jastadd;

import de.tudresden.inf.st.train.jastadd.config.ManualRelastIncrementalBenchmarkConfig;

public class ManualRelastIncrementalBenchmarkScenario extends
  JastaddAbstractBenchmarkScenario<ManualRelastIncrementalBenchmarkConfig> {

  public ManualRelastIncrementalBenchmarkScenario(final ManualRelastIncrementalBenchmarkConfig bc) throws Exception {
    super(bc, false);
  }

}
