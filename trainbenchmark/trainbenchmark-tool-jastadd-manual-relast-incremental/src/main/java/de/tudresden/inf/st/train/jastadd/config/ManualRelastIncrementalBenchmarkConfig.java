package de.tudresden.inf.st.train.jastadd.config;

import hu.bme.mit.trainbenchmark.benchmark.config.BenchmarkConfigBase;

public class ManualRelastIncrementalBenchmarkConfig extends JastaddAbstractBenchmarkConfig {

  protected ManualRelastIncrementalBenchmarkConfig(final BenchmarkConfigBase configBase) {
    super(configBase);
  }

  @Override
  public String getToolName() {
    return "Grammar Extension with Manual Serialization (Incremental)";
  }

  @Override
  public String getProjectName() {
    return "jastadd-manual-relast-incremental";
  }

}
