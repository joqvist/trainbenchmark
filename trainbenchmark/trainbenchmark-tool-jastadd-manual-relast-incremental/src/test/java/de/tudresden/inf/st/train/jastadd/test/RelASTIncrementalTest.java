package de.tudresden.inf.st.train.jastadd.test;

import de.tudresden.inf.st.train.jastadd.ManualRelastIncrementalBenchmarkScenario;
import de.tudresden.inf.st.train.jastadd.config.ManualRelastIncrementalBenchmarkConfig;
import de.tudresden.inf.st.train.jastadd.config.ManualRelastIncrementalBenchmarkConfigBuilder;
import hu.bme.mit.trainbenchmark.benchmark.config.BenchmarkConfigBase;
import hu.bme.mit.trainbenchmark.benchmark.runcomponents.BenchmarkResult;
import hu.bme.mit.trainbenchmark.benchmark.test.TrainBenchmarkTest;

public class RelASTIncrementalTest extends TrainBenchmarkTest {

  @Override
  protected BenchmarkResult runTest(final BenchmarkConfigBase bcb) throws Exception {
    final ManualRelastIncrementalBenchmarkConfig bc = new ManualRelastIncrementalBenchmarkConfigBuilder().setConfigBase(bcb).createConfig();
    final ManualRelastIncrementalBenchmarkScenario scenario = new ManualRelastIncrementalBenchmarkScenario(bc);
    final BenchmarkResult result = scenario.performBenchmark();
    return result;
  }

}
