package de.tudresden.inf.st.train.jastadd.test;

import de.tudresden.inf.st.train.jastadd.NameLookupIncrementalBenchmarkScenario;
import de.tudresden.inf.st.train.jastadd.config.NameLookupIncrementalBenchmarkConfig;
import de.tudresden.inf.st.train.jastadd.config.NameLookupIncrementalBenchmarkConfigBuilder;
import hu.bme.mit.trainbenchmark.benchmark.config.BenchmarkConfigBase;
import hu.bme.mit.trainbenchmark.benchmark.runcomponents.BenchmarkResult;
import hu.bme.mit.trainbenchmark.benchmark.test.TrainBenchmarkTest;

public class JastaddIncrementalTest extends TrainBenchmarkTest {

  @Override
  protected BenchmarkResult runTest(final BenchmarkConfigBase bcb) throws Exception {
    final NameLookupIncrementalBenchmarkConfig bc = new NameLookupIncrementalBenchmarkConfigBuilder().setConfigBase(bcb).createConfig();
    final NameLookupIncrementalBenchmarkScenario scenario = new NameLookupIncrementalBenchmarkScenario(bc);
    final BenchmarkResult result = scenario.performBenchmark();
    return result;
  }

}
