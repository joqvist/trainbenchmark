package de.tudresden.inf.st.train.jastadd;

import de.tudresden.inf.st.train.jastadd.config.NameLookupBenchmarkConfig;

public class NameLookupBenchmarkScenario extends
  JastaddAbstractBenchmarkScenario<NameLookupBenchmarkConfig> {

  public NameLookupBenchmarkScenario(final NameLookupBenchmarkConfig bc) throws Exception {
    super(bc, true);
  }
}
