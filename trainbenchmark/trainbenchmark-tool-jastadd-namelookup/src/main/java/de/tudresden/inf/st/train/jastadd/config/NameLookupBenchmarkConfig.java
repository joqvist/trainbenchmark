package de.tudresden.inf.st.train.jastadd.config;

import hu.bme.mit.trainbenchmark.benchmark.config.BenchmarkConfigBase;

public class NameLookupBenchmarkConfig extends JastaddAbstractBenchmarkConfig {

  protected NameLookupBenchmarkConfig(final BenchmarkConfigBase configBase) {
    super(configBase);
  }

  @Override
  public String getToolName() {
    return "Name Lookup";
  }

  @Override
  public String getProjectName() {
    return "jastadd-namelookup";
  }

}
