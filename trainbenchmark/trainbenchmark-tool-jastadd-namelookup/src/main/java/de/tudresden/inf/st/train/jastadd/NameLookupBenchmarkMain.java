package de.tudresden.inf.st.train.jastadd;

import de.tudresden.inf.st.train.jastadd.config.NameLookupBenchmarkConfig;
import hu.bme.mit.trainbenchmark.benchmark.config.BenchmarkConfig;

public class NameLookupBenchmarkMain {
  public static void main(final String[] args) throws Exception {
    final NameLookupBenchmarkConfig bc = BenchmarkConfig.fromFile(args[0], NameLookupBenchmarkConfig.class);
    final NameLookupBenchmarkScenario scenario = new NameLookupBenchmarkScenario(bc);
    scenario.performBenchmark();
    scenario.printTraces();
  }
}
