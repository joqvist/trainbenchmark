/**
 * Created by rschoene on 5/11/17.
 */

package de.tudresden.inf.st.trainbenchmark.generator.jastadd;

import de.tudresden.inf.st.trainbenchmark.generator.jastadd.config.ManualGeneratorConfig;
import hu.bme.mit.trainbenchmark.generator.ModelGenerator;
import hu.bme.mit.trainbenchmark.generator.ScalableGeneratorFactory;
import hu.bme.mit.trainbenchmark.generator.config.GeneratorConfig;

public class ManualGeneratorMain {
  public static void main(String[] args) throws Exception {
    final ManualGeneratorConfig gc = GeneratorConfig.fromFile(args[0], ManualGeneratorConfig.class);
    final ManualSerializer serializer = new ManualSerializer(gc);
    final ModelGenerator generator = ScalableGeneratorFactory.createGenerator(serializer, gc);
    generator.generateModel();
  }
}
