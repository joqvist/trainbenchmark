/**
 * Created by rschoene on 5/11/17.
 */

package de.tudresden.inf.st.trainbenchmark.generator.jastadd;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.module.SimpleModule;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;
import de.tudresden.inf.st.trainbenchmark.generator.jastadd.config.ManualGeneratorConfig;
import hu.bme.mit.trainbenchmark.generator.ModelSerializer;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static hu.bme.mit.trainbenchmark.constants.ModelConstants.*;

public class ManualSerializer extends ModelSerializer<ManualGeneratorConfig> {

  private static final Map<String, String> STRUCTURE_EDGE_MAP = new HashMap<String, String>() {{
    put(ELEMENTS, Children.TRACKELEMENT);
    put(SENSORS, Children.SENSOR);
    put(SEMAPHORES, Children.SEMAPHORE);
    put(FOLLOWS, Children.SWITCHPOSITION);
  }};
  private static final Map<String, String> REFERENCE_EDGE_MAP = new HashMap<String, String>() {{
    put(REQUIRES, Children.REQUIREDSENSOR);
    put(CONNECTS_TO, Children.CONNECTSTO);
  }};
  private ASTNonterminal container;

  public ManualSerializer(final ManualGeneratorConfig generatorConfig) {
    super(generatorConfig);
  }

  private ASTNonterminal makeRef(int targetId, String type) {
    ASTNonterminal refnt = new ASTNonterminal(type);
    refnt.addChild(Children.VALUE, new ASTTerminal("int", targetId));
    return refnt;
  }

  private ASTNonterminal makeRef(ASTNonterminal target, String type) {
    int id = (int) ((ASTTerminal) target.getChildren().get(Children.ID)).getValue();
    return makeRef(id, type);
  }

  @Override
  public String syntax() {
    return "JASTADD-MANUAL";
  }

  @Override
  public void initModel() throws IOException {
    container = new ASTNonterminal(Grammar.RAILWAYCONTAINER);
    container.getChildren().put(Grammar.ROUTE, new ASTList());
    container.getChildren().put(Grammar.REGION, new ASTList());
  }

  @Override
  public void persistModel() throws IOException {

    // Setup mapper with serializers
    ObjectMapper mapper = new ObjectMapper();
    mapper.disable(SerializationFeature.INDENT_OUTPUT);
//        mapper.enable(SerializationFeature.ORDER_MAP_ENTRIES_BY_KEYS);
    SimpleModule module = new SimpleModule();
    module.addSerializer(ASTNonterminal.class, new ASTNonterminalSerializer());
    module.addSerializer(ASTList.class, new ASTListSerializer());
    module.addSerializer(ASTOpt.class, new ASTOptSerializer());
    module.addSerializer(ASTTerminal.class, new ASTTerminalSerializer());
    module.addSerializer(ASTEnum.class, new ASTEnumSerializer());
    mapper.registerModule(module);

    // write out JSON
    String jsonPath = gc.getConfigBase().getModelPathWithoutExtension() + "-jastadd-manual.json";
    System.out.println(jsonPath);
    mapper.writeValue(new File(jsonPath), container);
  }

  @Override
  public Object createVertex(
    final int id,
    final String type,
    final Map<String, ? extends Object> attributes,
    final Map<String, Object> outgoingEdges,
    final Map<String, Object> incomingEdges) throws IOException {

    ASTNonterminal n = new ASTNonterminal(id, type);
    switch (type) {
      case REGION:
        n.createListChild(Children.TRACKELEMENT).createListChild(Children.SENSOR);
        ASTList regionList = (ASTList) container.getChildren().get(Grammar.REGION);
        regionList.addChild(n);
        break;
      case SEMAPHORE:
        if (attributes.get(SIGNAL) != null) {
          n.addChild(Children.SIGNAL, new ASTEnum(Values.ENUM_SIGNAL, attributes.get(SIGNAL)));
        }
        break;
      case ROUTE:
        n.createListChild(Children.SWITCHPOSITION).createListChild(Children.REQUIREDSENSOR);
        if (attributes.get(ACTIVE) != null) {
          n.addChild(Children.ACTIVE, new ASTTerminal("boolean", attributes.get(ACTIVE)));
        }
        if (outgoingEdges.get(ENTRY) != null) {
          n.addOptChild(Children.ENTRY, makeRef((ASTNonterminal) outgoingEdges.get(ENTRY), Grammar.SEMAPHORE_REF));
        } else {
          n.addOptChild(Children.ENTRY);
        }
        if (outgoingEdges.get(EXIT) != null) {
          n.addOptChild(Children.EXIT, makeRef((ASTNonterminal) outgoingEdges.get(EXIT), Grammar.SEMAPHORE_REF));
        } else {
          n.addOptChild(Children.EXIT);
        }
        ASTList routeList = (ASTList) container.getChildren().get(Grammar.ROUTE);
        routeList.addChild(n);
        break;
      case SWITCHPOSITION:
        if (attributes.get(POSITION) != null) {
          n.addChild(Children.POSITION, new ASTEnum(Values.ENUM_POSITION, attributes.get(POSITION)));
        }
        if (outgoingEdges.containsKey(TARGET)) {
          n.addChild(Children.TARGET, makeRef((ASTNonterminal) outgoingEdges.get(TARGET), Grammar.SWITCH_REF));
        }
        break;
      case SENSOR:
        n.createListChild(Children.MONITOREDELEMENT);
        break;
      case SEGMENT:
        n.createListChild(Children.CONNECTSTO).createListChild(Children.SEMAPHORE);
        if (attributes.get(LENGTH) != null) {
          n.addChild(Children.LENGTH, new ASTTerminal("int", attributes.get(LENGTH)));
        }
        break;
      case SWITCH:
        n.createListChild(Children.CONNECTSTO);
        break;
      default:
        System.err.println(String.format("Unknown type of vertex: %s", type));
    }
    return n;
  }

  @Override
  public void createEdge(final String label, final Object from, final Object to) throws IOException {

    ASTNonterminal to_nt = (ASTNonterminal) to;
    ASTNonterminal from_nt = (ASTNonterminal) from;
    if (label.equals(MONITORED_BY)) {
      ASTList listNode = (ASTList) to_nt.getChildren().get(Children.MONITOREDELEMENT);
      if (from_nt.getType().equals(SWITCH)) {
        listNode.addChild(makeRef(from_nt, Grammar.SWITCH_REF));
      } else if (from_nt.getType().equals(SEGMENT)) {
        listNode.addChild(makeRef(from_nt, Grammar.SEGMENT_REF));
      } else {
        throw new RuntimeException("A monitors edge could not be created because '" + from_nt.getType() + "' is the wrong outgoing type.");
      }
    } else if (STRUCTURE_EDGE_MAP.containsKey(label)) {
      String realEdgeLabel = STRUCTURE_EDGE_MAP.get(label);
      ASTNonterminal n = (ASTNonterminal) from;
      ASTList listNode = (ASTList) n.getChildren().get(realEdgeLabel);
      listNode.addChild((ASTNode) to);
    } else {
      String realEdgeLabel = REFERENCE_EDGE_MAP.get(label);
      ASTNonterminal n = (ASTNonterminal) from;
      ASTList listNode = (ASTList) n.getChildren().get(realEdgeLabel);
      if (label.equals(REQUIRES)) {
        listNode.addChild(makeRef((ASTNonterminal) to, Grammar.SENSOR_REF));
      } else if (label.equals(CONNECTS_TO)) {
        if (to_nt.getType().equals(SWITCH)) {
          listNode.addChild(makeRef(to_nt, Grammar.SWITCH_REF));
        } else if (to_nt.getType().equals(SEGMENT)) {
          listNode.addChild(makeRef(to_nt, Grammar.SEGMENT_REF));
        }
      } else {
        throw new RuntimeException("An edge could not be created because '" + label + "' is an umknown label.");
      }
    }
  }

  @Override
  public void setAttribute(final String type, final Object node, final String key, final Object value)
    throws IOException {
    if (!key.equals(CURRENTPOSITION)) {
      System.err.println(String.format("SetAttribute. Unexpected key: %s", type));
      return;
    }
    ASTNonterminal n = (ASTNonterminal) node;
    n.getChildren().put(Children.CURRENTPOSITION, new ASTEnum(Values.ENUM_POSITION, value));
  }

  static class Grammar {
    static final String RAILWAYCONTAINER = "RailwayContainer";
    static final String RAILWAYELEMENT = "RailwayElement";
    static final String REGION = "Region";
    static final String SEMAPHORE = "Semaphore";
    static final String ROUTE = "Route";
    static final String SWITCHPOSITION = "SwitchPosition";
    static final String SENSOR = "Sensor";
    static final String TRACKELEMENT = "TrackElement";
    static final String SEGMENT = "Segment";
    static final String SWITCH = "Switch";
    static final String NAME = "Name";
    static final String SENSOR_REF = "SensorRef";
    static final String SEMAPHORE_REF = "SemaphoreRef";
    static final String TRACKELEMENT_REF = "TrackElementRef";
    static final String SWITCH_REF = "SwitchRef";
    static final String SEGMENT_REF = "SegmentRef";
  }

  static class Children {
    static final String ROUTE = "Route";
    static final String REGION = "Region";
    static final String ID = "Id";
    static final String TRACKELEMENT = "TrackElement";
    static final String SENSOR = "Sensor";
    static final String SIGNAL = "Signal";
    static final String ACTIVE = "Active";
    static final String SWITCHPOSITION = "SwitchPosition";
    static final String REQUIREDSENSOR = "RequiredSensor";
    static final String ENTRY = "Entry";
    static final String EXIT = "Exit";
    static final String POSITION = "Position";
    static final String TARGET = "Target";
    static final String MONITOREDELEMENT = "MonitoredElement";
    static final String CONNECTSTO = "ConnectedElement";
    static final String LENGTH = "Length";
    static final String SEMAPHORE = "Semaphore";
    static final String CURRENTPOSITION = "CurrentPosition";
    static final String VALUE = "Value";
  }

  static class Values {
    static final String ENUM_SIGNAL = "de.tudresden.inf.st.train.jastadd.ast.Signal";
    static final String ENUM_POSITION = "de.tudresden.inf.st.train.jastadd.ast.Position";
  }

  private abstract class ASTNode {
    private String kind;
    private String type;

    ASTNode(String kind, String type) {
      this.kind = kind;
      this.type = type;
    }

    public String getKind() {
      return kind;
    }

    public void setKind(String kind) {
      this.kind = kind;
    }

    public String getType() {
      return type;
    }

    public void setType(String type) {
      this.type = type;
    }
  }

  private class ASTNonterminal extends ASTNode {
    private final Map<String, ASTNode> children;

    ASTNonterminal(String _type) {
      super("NT", _type);
      children = new HashMap<>();
    }

    ASTNonterminal(int id, String _type) {
      super("NT", _type);
      children = new HashMap<>();
      this.addChild(Children.ID, new ASTTerminal("int", id));
    }

    Map<String, ASTNode> getChildren() {
      return children;
    }

    ASTNonterminal addChild(String key, ASTNode n) {
      getChildren().put(key, n);
      return this;
    }

    ASTNonterminal createListChild(String context) {
      addChild(context, new ASTList());
      return this;
    }

    ASTNonterminal addOptChild(String context) {
      addChild(context, new ASTOpt());
      return this;
    }

    ASTNonterminal addOptChild(String context, ASTNode n) {
      addChild(context, new ASTOpt(n));
      return this;
    }
  }

  private class ASTList extends ASTNode {
    private final List<ASTNode> children;

    ASTList() {
      super("List", null);
      children = new ArrayList<>();
    }

    List<ASTNode> getChildren() {
      return children;
    }

    ASTList addChild(ASTNode n) {
      getChildren().add(n);
      return this;
    }
  }

  private class ASTOpt extends ASTNode {
    private ASTNode child;

    ASTOpt() {
      super("Opt", null);
      child = null;
    }

    ASTOpt(ASTNode child) {
      super("Opt", null);
      this.child = child;
    }

    boolean hasChild() {
      return child != null;
    }

    ASTNode getChild() {
      return child;
    }

    ASTOpt addChild(ASTNode n) {
      child = n;
      return this;
    }
  }

  private class ASTTerminal extends ASTNode {
    private final Object value;

    ASTTerminal(String _type, Object value) {
      super("t", _type);
      this.value = value;
    }

    public Object getValue() {
      return value;
    }

  }

  private class ASTEnum extends ASTNode {
    private final Object value;

    ASTEnum(String _type, Object value) {
      super("enum", _type);
      this.value = value;
    }

    public Object getValue() {
      return value;
    }
  }

  public class ASTNonterminalSerializer extends StdSerializer<ASTNonterminal> {
    public ASTNonterminalSerializer() {
      this(null);
    }

    public ASTNonterminalSerializer(Class<ASTNonterminal> t) {
      super(t);
    }

    @Override
    public void serialize(ASTNonterminal value, JsonGenerator gen, SerializerProvider provider) throws IOException {
      gen.writeStartObject();
      gen.writeStringField("k", value.getKind());
      gen.writeStringField("t", value.getType());
      gen.writeObjectField("c", value.getChildren());
      gen.writeEndObject();
    }
  }

  public class ASTListSerializer extends StdSerializer<ASTList> {
    public ASTListSerializer() {
      this(null);
    }

    public ASTListSerializer(Class<ASTList> t) {
      super(t);
    }

    @Override
    public void serialize(ASTList value, JsonGenerator gen, SerializerProvider provider) throws IOException {
      gen.writeStartObject();
      gen.writeStringField("k", value.getKind());
      gen.writeObjectField("c", value.getChildren());
      gen.writeEndObject();
    }
  }

  public class ASTOptSerializer extends StdSerializer<ASTOpt> {
    public ASTOptSerializer() {
      this(null);
    }

    public ASTOptSerializer(Class<ASTOpt> t) {
      super(t);
    }

    @Override
    public void serialize(ASTOpt value, JsonGenerator gen, SerializerProvider provider) throws IOException {
      gen.writeStartObject();
      gen.writeStringField("k", value.getKind());
      if (value.hasChild()) {
        gen.writeObjectField("c", value.getChild());
      }
      gen.writeEndObject();
    }
  }

  public class ASTTerminalSerializer extends StdSerializer<ASTTerminal> {
    public ASTTerminalSerializer() {
      this(null);
    }

    public ASTTerminalSerializer(Class<ASTTerminal> t) {
      super(t);
    }

    @Override
    public void serialize(ASTTerminal value, JsonGenerator gen, SerializerProvider provider) throws IOException {
      gen.writeStartObject();
      gen.writeStringField("k", value.getKind());
      gen.writeObjectField("t", value.getType());
      gen.writeObjectField("v", value.getValue());
      gen.writeEndObject();
    }

  }

  public class ASTEnumSerializer extends StdSerializer<ASTEnum> {
    public ASTEnumSerializer() {
      this(null);
    }

    public ASTEnumSerializer(Class<ASTEnum> t) {
      super(t);
    }

    @Override
    public void serialize(ASTEnum value, JsonGenerator gen, SerializerProvider provider) throws IOException {
      gen.writeStartObject();
      gen.writeStringField("k", value.getKind());
      gen.writeObjectField("t", value.getType());
      gen.writeObjectField("v", value.getValue());
      gen.writeEndObject();
    }

  }

}
