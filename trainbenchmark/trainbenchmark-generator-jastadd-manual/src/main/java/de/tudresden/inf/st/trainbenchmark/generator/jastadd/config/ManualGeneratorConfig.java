package de.tudresden.inf.st.trainbenchmark.generator.jastadd.config;

/**
 * Created by rschoene on 5/11/17.
 */

import hu.bme.mit.trainbenchmark.generator.config.GeneratorConfig;
import hu.bme.mit.trainbenchmark.generator.config.GeneratorConfigBase;

public class ManualGeneratorConfig extends GeneratorConfig {

  protected ManualGeneratorConfig(final GeneratorConfigBase configBase) {
    super(configBase);
  }

  @Override
  public String getProjectName() {
    return "jastadd-manual";
  }
}
