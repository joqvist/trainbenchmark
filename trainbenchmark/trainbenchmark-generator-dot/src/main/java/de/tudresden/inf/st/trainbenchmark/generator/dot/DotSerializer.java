/**
 * Created by rschoene on 5/11/17.
 */

package de.tudresden.inf.st.trainbenchmark.generator.dot;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;
import java.util.stream.Collectors;

import com.google.common.base.Joiner;
import de.tudresden.inf.st.trainbenchmark.generator.dot.config.DotGeneratorConfig;
import hu.bme.mit.trainbenchmark.generator.ModelSerializer;

public class DotSerializer extends ModelSerializer<DotGeneratorConfig> {

    private class Vertex {
        String type;
        Map<String, Object> attributes;
        Vertex(String type, Map<String, ? extends Object> attributes) {
            this.type = type;
//            this.attributes = attributes;
            this.attributes = new HashMap<>();
            this.attributes.putAll(attributes);
        }
    }

    BufferedWriter writer;
    Map<Integer, Vertex> vertices = new TreeMap<>();

    public DotSerializer(final DotGeneratorConfig generatorConfig) {
        super(generatorConfig);
    }

    @Override
    public String syntax() {
        return "Dot";
    }

    @Override
    public void initModel() throws IOException {
        String rawPath = gc.getConfigBase().getModelPathWithoutExtension() + ".dot";
        final File rawFile = new File(rawPath);
        writer = new BufferedWriter(new FileWriter(rawFile, false));
        writer.write("digraph G {\n" +
                "mindist = \"0.01\";\n" +
                "overlap = false;\n" +
                "edge [len = 0.01];\n" +
                "node [shape = \"record\"]\n\n");
    }

    @Override
    public void persistModel() throws IOException {
//        writer.write("Persist.\n");
        for (Map.Entry<Integer, Vertex> e : vertices.entrySet()) {
            Vertex v = e.getValue();
            String attString = v.attributes.entrySet().stream()
                    .map(entry -> '{' + entry.getKey() + ": " + entry.getValue() + '}')
                    .collect(Collectors.joining("|"));
            writer.write(String.format("v%d [label = \"{ %s | %s}\"]\n",
                    e.getKey(), v.type, attString));
        }
        writer.write("\n}\n");
        writer.close();
    }

    @Override
    public Object createVertex(
            final int id,
            final String type,
            final Map<String, ? extends Object> attributes,
            final Map<String, Object> outgoingEdges,
            final Map<String, Object> incomingEdges) throws IOException {
//        writer.write(String.format("Vertex. id: %s, type: %s, attributes: %s, outgoing: %s, incoming: %s\n", id, type, attributes, outgoingEdges, incomingEdges));
        // RootGroup1Sensor1 [label = "{ Sensor | { t | { name:senseo }|{ type:tt } } | { a | { mqtt-name:1g/senseo }|{ uniq-name:RootGroup1Sensor1 } } }"]
        vertices.put(id, new Vertex(type, attributes));
        return Integer.valueOf(id);
    }

    @Override
    public void createEdge(final String label, final Object from, final Object to) throws IOException {
//        writer.write(String.format("Edge. label: %s, from: %s, to: %s\n", label, from, to));
        writer.write(String.format("v%s -> v%s [label = \"s\"]\n", from, to, label));
    }


    @Override
    public void setAttribute(final String type, final Object node, final String key, final Object value)
            throws IOException {
//        writer.write(String.format("Attribute. type: %s, node: %s, key: %s, value: %s\n", type, node, key, value));
        vertices.get(node).attributes.put(key, value);
    }

}
