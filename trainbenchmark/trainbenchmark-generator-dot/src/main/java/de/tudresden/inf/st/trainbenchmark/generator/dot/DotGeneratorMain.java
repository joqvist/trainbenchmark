/**
 * Created by rschoene on 5/11/17.
 */

package de.tudresden.inf.st.trainbenchmark.generator.dot;

import de.tudresden.inf.st.trainbenchmark.generator.dot.config.DotGeneratorConfig;
import hu.bme.mit.trainbenchmark.generator.ModelGenerator;
import hu.bme.mit.trainbenchmark.generator.ModelSerializer;
import hu.bme.mit.trainbenchmark.generator.ScalableGeneratorFactory;
import hu.bme.mit.trainbenchmark.generator.config.GeneratorConfig;

public class DotGeneratorMain {
	public static void main(String[] args) throws Exception {
		final DotGeneratorConfig gc = GeneratorConfig.fromFile(args[0], DotGeneratorConfig.class);
		final DotSerializer serializer = new DotSerializer(gc);
		final ModelGenerator generator = ScalableGeneratorFactory.createGenerator(serializer, gc);
		generator.generateModel();
	}
}
