/**
 * Created by rschoene on 5/11/17.
 */
package de.tudresden.inf.st.trainbenchmark.generator.dot.config;

import hu.bme.mit.trainbenchmark.generator.config.GeneratorConfigBuilder;

public class DotGeneratorConfigBuilder
	extends GeneratorConfigBuilder<DotGeneratorConfig, DotGeneratorConfigBuilder> {

	@Override
	public DotGeneratorConfig createConfig() {
		checkNotNulls();
		return new DotGeneratorConfig(configBase);
	}
}
