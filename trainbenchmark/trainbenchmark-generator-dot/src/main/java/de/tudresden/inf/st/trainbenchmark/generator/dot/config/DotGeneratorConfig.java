package de.tudresden.inf.st.trainbenchmark.generator.dot.config;

/**
 * Created by rschoene on 5/11/17.
 */

import hu.bme.mit.trainbenchmark.generator.config.GeneratorConfig;
import hu.bme.mit.trainbenchmark.generator.config.GeneratorConfigBase;

public class DotGeneratorConfig extends GeneratorConfig {

	protected DotGeneratorConfig(final GeneratorConfigBase configBase) {
		super(configBase);
	}

	@Override
	public String getProjectName() {
		return "dot";
	}
}
