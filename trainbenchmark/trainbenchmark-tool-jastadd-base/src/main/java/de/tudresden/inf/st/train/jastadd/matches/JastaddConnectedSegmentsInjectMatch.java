package de.tudresden.inf.st.train.jastadd.matches;


import de.tudresden.inf.st.train.jastadd.ast.Segment;
import de.tudresden.inf.st.train.jastadd.ast.Sensor;
import hu.bme.mit.trainbenchmark.benchmark.matches.ConnectedSegmentsInjectMatch;

public class JastaddConnectedSegmentsInjectMatch extends JastaddMatch implements ConnectedSegmentsInjectMatch {

  protected final Sensor sensor;
  protected final Segment segment1;
  protected final Segment segment3;

  public JastaddConnectedSegmentsInjectMatch(final Sensor sensor, final Segment segment1, final Segment segment3) {
    super();
    this.sensor = sensor;
    this.segment1 = segment1;
    this.segment3 = segment3;
  }

  @Override
  public Sensor getSensor() {
    return sensor;
  }

  @Override
  public Segment getSegment1() {
    return segment1;
  }

  @Override
  public Segment getSegment3() {
    return segment3;
  }

}
