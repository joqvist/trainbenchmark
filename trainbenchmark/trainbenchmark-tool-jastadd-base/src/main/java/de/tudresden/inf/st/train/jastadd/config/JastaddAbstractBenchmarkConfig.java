package de.tudresden.inf.st.train.jastadd.config;

import hu.bme.mit.trainbenchmark.benchmark.config.BenchmarkConfig;
import hu.bme.mit.trainbenchmark.benchmark.config.BenchmarkConfigBase;

public abstract class JastaddAbstractBenchmarkConfig extends BenchmarkConfig {

  protected JastaddAbstractBenchmarkConfig(final BenchmarkConfigBase configBase) {
    super(configBase);
  }

}
