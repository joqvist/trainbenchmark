package de.tudresden.inf.st.train.jastadd.matches;

import de.tudresden.inf.st.train.jastadd.ast.Route;
import de.tudresden.inf.st.train.jastadd.ast.Sensor;
import de.tudresden.inf.st.train.jastadd.ast.Switch;
import de.tudresden.inf.st.train.jastadd.ast.SwitchPosition;
import hu.bme.mit.trainbenchmark.benchmark.matches.RouteSensorMatch;

public class JastaddRouteSensorMatch extends JastaddMatch implements RouteSensorMatch {

  protected final Route route;
  protected final Sensor sensor;
  protected final SwitchPosition swP;
  protected final Switch sw;

  public JastaddRouteSensorMatch(final Route route, final Sensor sensor, final SwitchPosition swP, final Switch sw) {
    super();
    this.route = route;
    this.sensor = sensor;
    this.swP = swP;
    this.sw = sw;
  }

  @Override
  public String toString() {
    return "[RouteSensorMatch Route:" + route.id() + " Sensor:" + sensor.id() + " SwitchPos:"
      + swP.id() + " Switch:" + sw.id() + "]";
  }

  @Override
  public Route getRoute() {
    return route;
  }

  @Override
  public Sensor getSensor() {
    return sensor;
  }

  @Override
  public SwitchPosition getSwP() {
    return swP;
  }

  @Override
  public Switch getSw() {
    return sw;
  }

}
