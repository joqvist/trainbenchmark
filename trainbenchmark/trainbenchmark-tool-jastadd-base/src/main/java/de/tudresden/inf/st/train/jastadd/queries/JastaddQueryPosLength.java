package de.tudresden.inf.st.train.jastadd.queries;

import de.tudresden.inf.st.train.jastadd.driver.JastaddDriver;
import de.tudresden.inf.st.train.jastadd.matches.JastaddPosLengthMatch;
import hu.bme.mit.trainbenchmark.constants.RailwayQuery;

import java.util.Collection;

public class JastaddQueryPosLength<TJastaddDriver extends JastaddDriver> extends JastaddQuery<JastaddPosLengthMatch> {

  public JastaddQueryPosLength(final TJastaddDriver driver) {
    super(RailwayQuery.POSLENGTH, driver);
  }

  @Override
  public Collection<JastaddPosLengthMatch> evaluate() {

    final Collection<JastaddPosLengthMatch> segments = driver.getModel().posLengthMatches();

    return segments;
  }
}
