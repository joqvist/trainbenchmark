package de.tudresden.inf.st.train.jastadd.matches;

import de.tudresden.inf.st.train.jastadd.ast.Segment;
import hu.bme.mit.trainbenchmark.benchmark.matches.PosLengthInjectMatch;

public class JastaddPosLengthInjectMatch extends JastaddMatch implements PosLengthInjectMatch {

  protected final Segment segment;

  public JastaddPosLengthInjectMatch(final Segment segment) {
    super();
    this.segment = segment;
  }

  @Override
  public Segment getSegment() {
    return segment;
  }

}
