package de.tudresden.inf.st.train.jastadd.matches;

import de.tudresden.inf.st.train.jastadd.ast.Switch;
import hu.bme.mit.trainbenchmark.benchmark.matches.SwitchMonitoredMatch;

public class JastaddSwitchMonitoredMatch extends JastaddMatch implements SwitchMonitoredMatch {

  protected final Switch sw;

  public JastaddSwitchMonitoredMatch(final Switch sw) {
    super();
    this.sw = sw;
  }

  @Override
  public Switch getSw() {
    return sw;
  }

}
