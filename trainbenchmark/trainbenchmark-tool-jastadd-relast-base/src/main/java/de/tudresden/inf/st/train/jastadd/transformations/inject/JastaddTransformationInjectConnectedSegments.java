package de.tudresden.inf.st.train.jastadd.transformations.inject;

import de.tudresden.inf.st.train.jastadd.ast.*;
import de.tudresden.inf.st.train.jastadd.driver.JastaddDriver;
import de.tudresden.inf.st.train.jastadd.matches.JastaddConnectedSegmentsInjectMatch;
import de.tudresden.inf.st.train.jastadd.transformations.JastaddTransformation;
import hu.bme.mit.trainbenchmark.constants.TrainBenchmarkConstants;

import java.util.Collection;

public class JastaddTransformationInjectConnectedSegments<TJastaddDriver extends JastaddDriver>
  extends JastaddTransformation<JastaddConnectedSegmentsInjectMatch, TJastaddDriver> {

  public JastaddTransformationInjectConnectedSegments(final TJastaddDriver driver) {
    super(driver);
  }

  @Override
  public void activate(final Collection<JastaddConnectedSegmentsInjectMatch> matches) {
    for (final JastaddConnectedSegmentsInjectMatch match : matches) {
      // create segment2
      Segment segment2 = new Segment();
      segment2.setId(driver.nextId());
      segment2.addToConnectsTo(match.getSegment3());
      segment2.setLength(TrainBenchmarkConstants.DEFAULT_SEGMENT_LENGTH);

      match.getSegment1().containingRegion().addTrackElement(segment2);

      // have the sensor monitor the segment
      match.getSensor().addToMonitors(segment2);

      // remove the connection of segment1 to segment3
      match.getSegment1().removeFromConnectsTo(match.getSegment3());

      // connect segment1 to segment2
      match.getSegment1().addToConnectsTo(segment2);
    }
    driver.flushCache();
  }

}
