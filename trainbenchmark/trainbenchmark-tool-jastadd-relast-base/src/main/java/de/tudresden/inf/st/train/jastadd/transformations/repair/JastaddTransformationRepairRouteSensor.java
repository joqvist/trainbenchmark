package de.tudresden.inf.st.train.jastadd.transformations.repair;

import de.tudresden.inf.st.train.jastadd.ast.Route;
import de.tudresden.inf.st.train.jastadd.ast.Sensor;
import de.tudresden.inf.st.train.jastadd.driver.JastaddDriver;
import de.tudresden.inf.st.train.jastadd.matches.JastaddRouteSensorMatch;
import de.tudresden.inf.st.train.jastadd.transformations.JastaddTransformation;

import java.util.Collection;

public class JastaddTransformationRepairRouteSensor<TJastaddDriver extends JastaddDriver>
  extends JastaddTransformation<JastaddRouteSensorMatch, TJastaddDriver> {

  public JastaddTransformationRepairRouteSensor(final TJastaddDriver driver) {
    super(driver);
  }

  @Override
  public void activate(final Collection<JastaddRouteSensorMatch> matches) {
    for (final JastaddRouteSensorMatch rsm : matches) {
      final Route route = rsm.getRoute();
      final Sensor sensor = rsm.getSensor();
      route.addToRequires(sensor);
      driver.flushCache();
    }
  }

}
