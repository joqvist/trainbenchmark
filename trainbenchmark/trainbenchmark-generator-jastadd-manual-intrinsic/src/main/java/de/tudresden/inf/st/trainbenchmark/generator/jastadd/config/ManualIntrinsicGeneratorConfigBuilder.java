package de.tudresden.inf.st.trainbenchmark.generator.jastadd.config;

import hu.bme.mit.trainbenchmark.generator.config.GeneratorConfigBuilder;

public class ManualIntrinsicGeneratorConfigBuilder
	extends GeneratorConfigBuilder<ManualIntrinsicGeneratorConfig, ManualIntrinsicGeneratorConfigBuilder> {

	@Override
	public ManualIntrinsicGeneratorConfig createConfig() {
		checkNotNulls();
		return new ManualIntrinsicGeneratorConfig(configBase);
	}
}
