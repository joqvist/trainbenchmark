/**
 * Created by rschoene on 5/11/17.
 */

package de.tudresden.inf.st.trainbenchmark.generator.jastadd;

import de.tudresden.inf.st.trainbenchmark.generator.jastadd.config.ManualIntrinsicGeneratorConfig;
import hu.bme.mit.trainbenchmark.generator.ModelGenerator;
import hu.bme.mit.trainbenchmark.generator.ScalableGeneratorFactory;
import hu.bme.mit.trainbenchmark.generator.config.GeneratorConfig;

public class ManualIntrinsicGeneratorMain {
	public static void main(String[] args) throws Exception {
		final ManualIntrinsicGeneratorConfig gc = GeneratorConfig.fromFile(args[0], ManualIntrinsicGeneratorConfig.class);
		final ManualIntrinsicSerializer serializer = new ManualIntrinsicSerializer(gc);
		final ModelGenerator generator = ScalableGeneratorFactory.createGenerator(serializer, gc);
		generator.generateModel();
	}
}
