package de.tudresden.inf.st.train.jastadd.config;

import hu.bme.mit.trainbenchmark.benchmark.config.BenchmarkConfigBuilder;

public class ManualRelastBenchmarkConfigBuilder extends BenchmarkConfigBuilder<ManualRelastBenchmarkConfig, ManualRelastBenchmarkConfigBuilder> {

  @Override
  public ManualRelastBenchmarkConfig createConfig() {
    checkNotNulls();
    return new ManualRelastBenchmarkConfig(configBase);
  }

}
