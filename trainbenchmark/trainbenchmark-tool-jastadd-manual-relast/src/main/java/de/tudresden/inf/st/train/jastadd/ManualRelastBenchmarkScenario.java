package de.tudresden.inf.st.train.jastadd;

import de.tudresden.inf.st.train.jastadd.config.ManualRelastBenchmarkConfig;

public class ManualRelastBenchmarkScenario extends
  JastaddAbstractBenchmarkScenario<ManualRelastBenchmarkConfig> {

  public ManualRelastBenchmarkScenario(final ManualRelastBenchmarkConfig bc) throws Exception {
    super(bc, true);
  }

}
