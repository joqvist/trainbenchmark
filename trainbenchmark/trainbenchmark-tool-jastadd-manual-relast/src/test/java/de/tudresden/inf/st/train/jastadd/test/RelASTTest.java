package de.tudresden.inf.st.train.jastadd.test;

import de.tudresden.inf.st.train.jastadd.ManualRelastBenchmarkScenario;
import de.tudresden.inf.st.train.jastadd.config.ManualRelastBenchmarkConfig;
import de.tudresden.inf.st.train.jastadd.config.ManualRelastBenchmarkConfigBuilder;
import hu.bme.mit.trainbenchmark.benchmark.config.BenchmarkConfigBase;
import hu.bme.mit.trainbenchmark.benchmark.runcomponents.BenchmarkResult;
import hu.bme.mit.trainbenchmark.benchmark.test.TrainBenchmarkTest;

public class RelASTTest extends TrainBenchmarkTest {

  @Override
  protected BenchmarkResult runTest(final BenchmarkConfigBase bcb) throws Exception {
    final ManualRelastBenchmarkConfig bc = new ManualRelastBenchmarkConfigBuilder().setConfigBase(bcb).createConfig();
    final ManualRelastBenchmarkScenario scenario = new ManualRelastBenchmarkScenario(bc);
    final BenchmarkResult result = scenario.performBenchmark();
    return result;
  }

}
