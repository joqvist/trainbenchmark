package de.tudresden.inf.st.train.jastadd.transformations.inject;

import de.tudresden.inf.st.train.jastadd.ast.SensorRef;
import de.tudresden.inf.st.train.jastadd.ast.Sensor;
import de.tudresden.inf.st.train.jastadd.ast.List;
import de.tudresden.inf.st.train.jastadd.driver.JastaddDriver;
import de.tudresden.inf.st.train.jastadd.matches.JastaddRouteSensorInjectMatch;
import de.tudresden.inf.st.train.jastadd.transformations.JastaddTransformation;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.concurrent.TimeUnit;

public class JastaddTransformationInjectRouteSensor<TJastaddDriver extends JastaddDriver>
  extends JastaddTransformation<JastaddRouteSensorInjectMatch, TJastaddDriver> {

  public JastaddTransformationInjectRouteSensor(final TJastaddDriver driver) {
    super(driver);
  }

  @Override
  public void activate(final Collection<JastaddRouteSensorInjectMatch> matches) {
    java.util.List<SensorRef> refsToBeRemoved = new ArrayList<>();

    for (final JastaddRouteSensorInjectMatch match : matches) {
      for (SensorRef ref : match.getRoute().getRequiredSensors()) {
        if (ref.getSensor() == match.getSensor()) {
          refsToBeRemoved.add(ref);
        }
      }
    }

    for(SensorRef ref : refsToBeRemoved) {
      ref.removeSelf();
    }

    driver.flushCache();
  }

}
